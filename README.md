# TesteCompasso

- Implementado os casos de testes baseados nas APIs fornecidas;
- A idealização do projeto foi:
    - Uso do REST-Assured para implentação dos métodos das APIs;
    - Uso do Cucumber para escrita em Ghenkin em português, facilitando assim a leitura e manutenção dos testes;
    - Uso do JUnit como auxiliar na execução dos testes;
Observe que o Caso de Teste de consulta de simulação existente está retornando erro, pois o StatusCode retornada diverge do indicado no Swagger.

Dúvidas à disposição.